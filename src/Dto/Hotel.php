<?php

namespace App\Dto;

use stdClass;

final class Hotel
{
    /**
     * @param int $id
     * @param string|null $name
     * @param string|null $website
     * @param string|null $email
     * @param string|null $phone
     * @param string|null $fax
     * @param string|null $address
     * @param string|null $description
     * @param int $stars
     * @param array $types
     * @param array $services
     * @param array $images
     */
    public function __construct(
        private readonly int $id,
        private readonly ?string $name,
        private readonly ?string $website,
        private readonly ?string $email,
        private readonly ?string $phone,
        private readonly ?string $fax,
        private readonly ?string $address,
        private readonly ?string $description,
        private readonly int $stars = 0,
        private readonly array $types = [],
        private readonly array $services = [],
        private readonly array $images = [],
    ) {
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return string|null
     */
    public function getWebsite(): ?string
    {
        return $this->website;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @return string|null
     */
    public function getFax(): ?string
    {
        return $this->fax;
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @return int
     */
    public function getStars(): int
    {
        return $this->stars;
    }

    /**
     * @return array
     */
    public function getTypes(): array
    {
        return $this->types;
    }

    /**
     * @return array
     */
    public function getServices(): array
    {
        return $this->services;
    }

    /**
     * @return array
     */
    public function getImages(): array
    {
        return $this->images;
    }

    /**
     * @return stdClass
     */
    public function getInfoForListing(): stdClass
    {
        return (object) [
            'id'    => $this->getId(),
            'name'  => $this->getName(),
            'stars' => $this->getStars(),
            'types' => $this->getTypes(),
        ];
    }
}
