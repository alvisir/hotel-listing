<?php

namespace App\Repository;

use App\Entity\Visualization;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Query\Parameter;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Visualization>
 */
class VisualizationRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Visualization::class);
    }

    /**
     * @param string $ip
     * @param string $path
     * @return bool
     */
    public function checkIfAlreadyVisited(string $ip, string $path): bool
    {
        $qb = $this->createQueryBuilder('v');
        $expr = $qb->expr();

        return (bool) $qb
                ->select('COUNT(v.id)')
                ->andWhere($expr->eq('v.ipAddress', ':ip'))
                ->andWhere($expr->eq('v.path', ':path'))
                ->andWhere($expr->gte('v.createdAt', ':today'))
                ->andWhere($expr->lte('v.createdAt', ':tomorrow'))
                ->setParameters(new ArrayCollection([
                    new Parameter('ip', $ip),
                    new Parameter('path', $path),
                    new Parameter('today', new \DateTimeImmutable('today')),
                    new Parameter('tomorrow', new \DateTimeImmutable('tomorrow'))
                ]))
                ->getQuery()
                ->getSingleScalarResult() > 0
        ;
    }
}
