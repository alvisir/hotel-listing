<?php

namespace App\Service\Api;

use App\Dto\Hotel;
use Psr\Cache\InvalidArgumentException;
use stdClass;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class HotelsApi
{
    public const MAX_IMAGES = 1;

    /**
     * $hotelsApiClient is configured in framework.yaml file
     *
     * @param HttpClientInterface $hotelsApiClient
     * @param CacheInterface $cachePool
     * @param int $cacheLifeTime
     * @param string $hotelImagesDir
     */
    public function __construct(
        private HttpClientInterface $hotelsApiClient,
        private CacheInterface $cachePool,
        private int $cacheLifeTime,
        private string $hotelImagesDir
    ) {
    }

    /**
     * @param string|null $search
     * @param array $orderBy
     * @param int $page
     * @return array|null
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws InvalidArgumentException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getHotelsList(?string $search = null, array $orderBy = [], int $page = 1): ?array
    {
        $start = ($page - 1) * 15;
        $end = $start + 14;

        $response = $this->hotelsApiClient->request(
            'GET',
            '/api/portali/lignanoit/presenti/hotel',
            [
                'query' => [...['filtri' => ['nome' => $search]], ...$orderBy],
                'headers' => ['Range' => sprintf('items=%s-%s', $start, $end)]
            ]
        );

        if (206 !== $response->getStatusCode()) {
            return null;
        }

        $results['list'] = array_map(
            function (int $hotelId): stdClass {
                return $this->getHotel($hotelId)->getInfoForListing();
            },
            array_column($response->toArray(), 'id'),
        );

        if (isset($response->getHeaders()['content-range'])) {
            $contentRange = explode('/', $response->getHeaders()['content-range'][0]);
            $total = (int) end($contentRange);

            $results['pagination'] = [
                'total' => $total,
                'numPages' => (int) ceil($total / 15),
                'page' => $page,
            ];
        }

        return $results;
    }

    /**
     * @param int $id
     * @return Hotel|null
     *
     * @throws InvalidArgumentException
     */
    public function getHotel(int $id): ?Hotel
    {
        return $this->cachePool->get(
            sprintf('hotel_%s', $id),
            function (ItemInterface $item) use ($id) {
                $item->expiresAfter($this->cacheLifeTime);

                $response = $this->hotelsApiClient->request('GET', sprintf('/api/hotel/hotel/%s', $id));

                if (200 !== $response->getStatusCode()) {
                    return null;
                }

                $hotelData = $response->toArray();

                $hotel = new Hotel(
                    id:          $hotelData['id'],
                    name:        $hotelData['esercizio']['nome'] ?? null,
                    website:     $hotelData['esercizio']['web'] ?? null,
                    email:       $hotelData['esercizio']['email'] ?? null,
                    phone:       $hotelData['esercizio']['telefono'] ?? null,
                    fax:         $hotelData['esercizio']['fax'] ?? null,
                    address: sprintf(
                        '%s, %s - %s',
                        $hotelData['esercizio']['indirizzo'] ?? null,
                        $hotelData['esercizio']['numero'] ?? null,
                        $hotelData['esercizio']['citta']['nome'] ?? null
                    ),
                    description: $hotelData['descrizione']['testo'] ?? null,
                    stars:       $hotelData['stelle'] ?? 0,
                    types:       array_column($hotelData['trattamenti'] ?? [], 'trattamento'),
                    services:    array_map(fn ($service) => $service['servizio']['nome'], $hotelData['servizi'] ?? []),
                    // save max the MAX_IMAGES first images
                    images:      isset($hotelData['fotografie']) ? array_map(fn (array $photo) => $photo['id'], array_slice($hotelData['fotografie'], 0, self::MAX_IMAGES)) : []
                );

                foreach ($hotel->getImages() as $imageId) {
                    $this->saveImage($hotel->getId(), $imageId);
                }

                return $hotel;
            }
        );
    }

    /**
     * @param int $hotelId
     * @param int $imageId
     * @return void|null
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function saveImage(int $hotelId, int $imageId)
    {
        $filesystem = new Filesystem();

        $path = sprintf('%s/%s', $this->hotelImagesDir, $hotelId);

        $filesystem->mkdir($path, 0700);

        $response = $this->hotelsApiClient->request('GET', sprintf('/api/hotel/hotel/%s/foto/%s', $hotelId, $imageId));

        if (200 !== $response->getStatusCode()) {
            return null;
        }

        file_put_contents(sprintf('%s/%s.jpg', $path, $imageId), $response->getContent());
    }
}
