<?php

namespace App\Controller;

use App\Config\Enum\SearchOrderBy;
use App\Form\SearchHotelsType;
use App\Service\Api\HotelsApi;
use Psr\Cache\InvalidArgumentException;
use Symfony\Bridge\Twig\Attribute\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class HotelsController extends AbstractController
{
    /**
     * @param HotelsApi $hotelsApi
     */
    public function __construct(private HotelsApi $hotelsApi)
    {
    }

    /**
     * @param Request $request
     * @return Response
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws InvalidArgumentException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    #[Route('/', name: 'app_hotels_index')]
    public function index(Request $request): Response
    {
        $page = (int) ($request->get('page') ?? 1);
        $search = null;
        $orderBy = [];
        $responseStatus = 200;
        $form = $this->createForm(SearchHotelsType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $search = $form->getData()['search'];
                $orderBy = SearchOrderBy::getLabel($form->getData()['orderBy']);
            } else {
                $responseStatus = 422;
            }
        }

        return $this->render(
            'hotels/index.html.twig',
            [
                'hotels' =>  $this->hotelsApi->getHotelsList($search, $orderBy, $page),
                'form' => $form,
            ],
            $request->isXmlHttpRequest() ?
            new Response(null, $responseStatus, [
                'Content-Type' => 'text/vnd.turbo-stream.html'
            ]) : null
        );
    }

    /**
     * @param int $id
     * @return array
     * @throws InvalidArgumentException
     */
    #[Route('/hotels/{id}', name: 'app_hotels_view')]
    #[Template('hotels/view.html.twig')]
    public function view(int $id): array
    {
        return [
            'hotel' => $this->hotelsApi->getHotel($id),
        ];
    }
}
