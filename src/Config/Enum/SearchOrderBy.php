<?php

namespace App\Config\Enum;

use Symfony\Contracts\Translation\TranslatableInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

enum SearchOrderBy: string implements TranslatableInterface
{
    case Name =      'name';
    case NameDesc =  'name_desc';
    case Stars =     'stars';
    case StarsDesc = 'stars_desc';

    public static function getLabel(self $value): array
    {
        return match ($value) {
            self::Name      => ['ordineTipo' => 'nome'],
            self::NameDesc  => ['ordineTipo' => 'nome', 'ordineDirezione' => 'DESC'],
            self::Stars     => ['ordineTipo' => 'stelle', 'ordineDirezione' => 'ASC'],
            self::StarsDesc => ['ordineTipo' => 'stelle', 'ordineDirezione' => 'DESC'],
        };
    }

    public function trans(TranslatorInterface $translator, ?string $locale = null): string
    {
        return match ($this) {
            self::Name  => $translator->trans('index.order_by_name', domain: 'hotels'),
            self::NameDesc => $translator->trans('index.order_by_name_desc', domain: 'hotels'),
            self::Stars  => $translator->trans('index.order_by_stars', domain: 'hotels'),
            self::StarsDesc  => $translator->trans('index.order_by_stars_desc', domain: 'hotels'),
        };
    }
}
