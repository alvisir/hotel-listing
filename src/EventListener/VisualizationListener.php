<?php

namespace App\EventListener;

use App\Entity\Visualization;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

final class VisualizationListener
{
    /**
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    #[AsEventListener(event: KernelEvents::REQUEST)]
    public function onKernelRequest(RequestEvent $event): void
    {
        if (!$event->isMainRequest()) {
            return;
        }

        $request = $event->getRequest();

        if ($request->isXmlHttpRequest()) {
            return;
        }

        $alreadyVisited = $this->entityManager
            ->getRepository(Visualization::class)
            ->checkIfAlreadyVisited(ip: $request->getClientIp(), path: $request->getPathInfo())
        ;

        if ($alreadyVisited) {
            return;
        }

        $visualization = (new Visualization())
            ->setIpAddress($request->getClientIp())
            ->setPath($request->getPathInfo())
            ->setUrl($request->getUri())
        ;

        $this->entityManager->persist($visualization);
        $this->entityManager->flush();
    }
}
