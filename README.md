# Hotel Listing

A simple web app in **Symfony 7** and **PostgreSQL** to show a list of hotels.
Hotels data is retrieved from an API and cached to avoid unnecessary consumption.

## Requirements
You can use <a href="https://docs.docker.com/get-docker/">Docker</a> or a PHP development environment like <a hrf="https://herd.laravel.com/">Herd</a>.
However, the first option is strongly recommended.
<a href="https://symfony.com/doc/current/setup.html#technical-requirements">Here</a> the Symfony official list of technical requirements.

## Docker Installation
- Clone this repo in your environment.
- Inside root directory run `docker compose build --no-cache --pull` and `docker compose up -d`.
- Run `docker compose exec node sh` to start interactive shell of node container and launch inside `npm install` to install node packages and `npm run dev` (or `npm run build`) to compile assets.
- If everything is ok you should be able to access your app via `http://localhost`.

This project use a modified version of https://github.com/dunglas/symfony-docker.

## Configuration

### API
In order to consume API from **mercuriosistemi.com**, you must auth by username and password.
Set the following variables in your `.env` file:
- `HOTELS_API_USER` (user)
- `HOTELS_API_PASSWORD` (password)

### Database
Default database settings:
- User: `app`
- Password: `!ChangeMe!`
- Database: `app`

If you want to use different settings edit the following environment variables in your `.env` file:
- `POSTGRES_USER` (user)
- `POSTGRES_PASSWORD` (password)
- `POSTGRES_DB` (database name)

Remember to update also `DATABASE_URL`.
**IMPORTANT**: for the changes to work you must rebuild the container.

The Docker container provides **Adminer**, a fancy database administration tool to inspect the visitors stats.
Go to `http://localhost:8080` to access it.

### Cache
By default, the API responses are cached for one hour (3600 seconds). To change the lifetime change `HOTELS_CACHE_LIFETIME` variable in your `.env` file.

## Technical Info
- Each hotel data retrieved by API is converted to a DTO with the needed properties and cached.
- The Listing is paginated with max 15 results for page (this limit cannot be changed)
- For each fetched hotel will be created a directory in `public/images` where the corresponding photo will be saved.
- <a href="https://tailwindcss.com/">TailwindCSS</a> is used for the interface.
