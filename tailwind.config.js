/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./assets/**/*.js",
    "./templates/**/*.html.twig",
  ],
  theme: {
    extend: {
      fontSize: {
        xxs: ['0.5rem', '0.75rem'],
      },
    },
  },
  plugins: [],
}
